﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public GameManager _gameManager;
    public Sprite[] spriteHappy;
    public Sprite spriteDie;
    private SpriteRenderer renderer;

	// Use this for initialization
	void Start () {
        renderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (_gameManager.gameState == GameState.play)
        {
            if (col.tag.Equals("enemy"))
            {
                die();
            }

            if (col.tag.Equals("bonus"))
            {
                Destroy(col.gameObject);
                takeBonus();
            }
        }
    }

    void takeBonus()
    {
        SoundManager.instance.playSoundBonus();
        _gameManager.bonus();
        changeSprite();
    }

    void changeSprite()
    {
        int random = Random.Range(0, spriteHappy.Length);
        renderer.sprite = spriteHappy[random];
    }

    void die()
    {
        SoundManager.instance.playSoundDie();
        Camera.main.GetComponent<ShakeCamera>().DoShake();
        renderer.sprite = spriteDie;
        _gameManager.gameOver();
    }
}
