﻿using UnityEngine;
using System.Collections;

public class EnemyGen : MonoBehaviour {

    public GameObject enemy;
    public float timeDelay = 1;

	// Use this for initialization
	void Start () {
        StartCoroutine(genEnemy());
	}

	IEnumerator genEnemy () {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            GameObject obj = (GameObject)Instantiate(enemy, transform.position, enemy.transform.rotation);
            obj.transform.parent = transform;
        }
	}
}
