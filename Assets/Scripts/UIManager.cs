﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public Text scoreText;
    public Text scoreOver;
    public Text bestScore;
    public GameObject tableOver;

	// Use this for initialization
	void Start () {
	
	}
	
	public void reset () {
        SoundManager.instance.soundBackgroundPlay();
        Application.LoadLevel(GameManager.GAMEPLAY_SCENE);
	}

    public void updateScore(int score)
    {
        scoreText.text = "" + score;
    }

    public void gameOver()
    {
        StartCoroutine(delayOver());
    }

    IEnumerator delayOver()
    {
        yield return new WaitForSeconds(1);
        SoundManager.instance.playSoundGameOver();
        scoreText.transform.parent.gameObject.SetActive(false);
        tableOver.SetActive(true);
        scoreOver.text = scoreText.text;
        bestScore.text = PlayerPrefs.GetInt(GameManager.BESTSCORE_KEY) + "";
        AdsControl.Instance.showAds();
    }

    public void mainMenu()
    {
        SoundManager.instance.soundBackgroundMenu();
        Application.LoadLevel(GameManager.MAINMENU_SCENE);
    }
}
