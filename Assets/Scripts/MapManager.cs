using UnityEngine;
using System.Collections;

public class MapManager : MonoBehaviour {

    public GameManager _gameManager;
	public GameObject[] map;
	public GameObject A_Zone;
	public GameObject B_Zone;
	
	public float Speed = 1f;
	
	void Update () {
	
			MOVE();
	}
	
	public void MAKE(){
		
		B_Zone=A_Zone;
		int a = Random.Range(0,map.Length);
        A_Zone = Instantiate(map[a], new Vector3(20,0,0), transform.rotation) as GameObject;
		
	}
	
	public void MOVE(){
        if (Input.GetMouseButton(0) && _gameManager.gameState == GameState.play)
        {
            A_Zone.transform.Translate(Vector3.left * Speed * Time.deltaTime, Space.World);
            B_Zone.transform.Translate(Vector3.left * Speed * Time.deltaTime, Space.World);
        }
			
		if(A_Zone.transform.position.x<=0){
				DEATH();
		}
	}
	
	public void DEATH(){
		Destroy(B_Zone);
		MAKE();	
	}
}
