﻿using UnityEngine;
using System.Collections;

public enum GameState {play,gameOver};

public class GameManager : MonoBehaviour {

    public const string BESTSCORE_KEY = "bestscore";
    public const string GAMEPLAY_SCENE = "GamePlay";
    public const string MAINMENU_SCENE = "MainMenu";

    public UIManager _uiManager;
    public GameState gameState;
    public int score;

	// Use this for initialization
	void Start () {
        setGameState(GameState.play);
    }
	
	public void setGameState(GameState state)
    {
        gameState = state;
    }

    public void bonus()
    {
        score++;
        _uiManager.updateScore(score);
    }

    public void gameOver()
    {
        _uiManager.gameOver();
        setGameState(GameState.gameOver);
        if (score > PlayerPrefs.GetInt(BESTSCORE_KEY))
        {
            PlayerPrefs.SetInt(BESTSCORE_KEY, score);
        }
    }
}
