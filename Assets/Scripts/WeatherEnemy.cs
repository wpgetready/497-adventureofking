﻿using UnityEngine;
using System.Collections;

public class WeatherEnemy : MonoBehaviour {

    public float point_Y_Destroy = -3;
    public float speed = 9;

    void Update()
    {
        float offset = speed * Time.deltaTime;

        transform.localPosition = new Vector2(transform.localPosition.x, transform.localPosition.y - offset);

        if (transform.position.y < point_Y_Destroy)
        {
            Destroy(gameObject);
        }
    }
}
