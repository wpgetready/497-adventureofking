﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Fish : MonoBehaviour {

    public float forceX;
    public float forceY;
    public float point_Y_Destroy = -5;
    // Use this for initialization
    void Start () {
        GetComponent<Rigidbody2D>().AddForce(new Vector2(forceX, forceY));
	}

    void Update()
    {
        if (transform.position.y < point_Y_Destroy)
        {
            Destroy(gameObject);
        }
    }
	
}
